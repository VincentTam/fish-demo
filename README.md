# Hugo Future Imperfect Slim + Staticman

Copy the `.gitlab-ci.yml` after installing
[Hugo Future Imperfect Slim](https://framagit.org/VincentTam/hugo-future-imperfect-slim)
to get GitLab Pages working.
