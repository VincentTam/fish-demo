+++
title = "Contact"
type = "contact"
layout = "contact"
netlify = false
emailservice = "formspree.io/example@email.com"
contactname = "Your name"
contactemail = "Your Email"
contactsubject = "Subject"
contactmessage = "Your Message"
contactlang = "fr"
contactanswertime = 24
+++
